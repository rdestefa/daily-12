console.log("Started main.js");

// Event trigger - react to button clicks
document.getElementById("submit_button").onmouseup = readAndPrint; 
document.getElementById("reset_button").onmouseup = clearForm;

// Event handler - Print statement
function readAndPrint() {
    console.log("Entered readAndPrint()");
    
    // Get values of input text
    var text1 = document.getElementById("input-text-1").value;
    var text2 = document.getElementById("input-text-2").value;
    var text3 = document.getElementById("input-text-3").value;
    
    // Get value of dropdown menu
    var dropdownVal = document.getElementById("select-1").value;
    
    // Get value of radio buttons
    var radios = document.getElementsByName("bsr-radios");
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked)
        var radio = radios[i].value;
    }
    
    // Get value of checkbox
    var checkedBox = document.getElementById("checkbox-1").checked;
    
    // Set output text
    outputString = text1 + " is a " + dropdownVal + " " + text3 + " major who lives in " + text2 + ". They prefer to eat in " + radio + " and ";
    
    if (checkedBox) {
        outputString = outputString + "they have a bike on campus.";
    } else {
        outputString = outputString + "they do not have a bike on campus.";
    }
    
    document.getElementById("textarea-1").value = outputString;
}

// Event handler - Clear form
function clearForm() {
    console.log("Entered clearForm()");
    
    // Clear form
    document.getElementById("input-text-1").value = "";
    document.getElementById("input-text-2").value = "";
    document.getElementById("input-text-3").value = "";
    document.getElementById("select-1").value = "Freshman";
    document.getElementById("checkbox-1").checked = false;
    
    var radios = document.getElementsByName("bsr-radios");
    
    for (var i = 0; i < radios.length; i++) {
        radios[i].checked = false;
    }
    
    document.getElementById("textarea-1").value = "";
}